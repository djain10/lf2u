package cs445.iit.edu.BoundaryInterfaces;

import java.util.List;


import cs445.iit.edu.models.Delivery;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.Productdetails;

public interface FarmerBoundaryInterface {

	String createFarmer(FarmerDetails f);

	boolean updateFarmer(String fid, FarmerDetails f);

	String getFarm(String fid);
	
	public List<FarmerDetails> viewAllFarmers();
	
	String zip(String zi);

	String productsList(String s);

	String createProd(String s, Productdetails prod);

	boolean updateProductInfo(String s, String s1, Productdetails pd);

	String getProductDetails(String s, String s1);

	String getReportList();


	boolean deliveryCharge(String s, Delivery d1);

	String getDeliveryCharges(String s);

	String getReport(String s, int s1);

	String getReport1(String s, int s1, String st1, String st2);
	String getReport1(String s, int s1);

	
}
