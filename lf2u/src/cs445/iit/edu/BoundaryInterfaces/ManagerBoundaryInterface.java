package cs445.iit.edu.BoundaryInterfaces;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import cs445.iit.edu.models.CatalogView;

public interface ManagerBoundaryInterface {

   public String getmlist();
   public String getmanager(int mid);
   public String getreplist();
public String getManagerReport1( int s1,String st1, String st2);
public String getManagerReport2( int s1, String st1, String st2);
public String increCatalog(CatalogView use1);
public String haveCatalog();
public boolean updateCatalog(String s, CatalogView d) throws JsonParseException, JsonMappingException, IOException;

}
