package cs445.iit.edu.BoundaryInterfaces;

import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.PlaceOrder;

public interface CustomerBoundaryInterface {

	public String createAccount(CustomerDetails h);

	public boolean update(String s, CustomerDetails d);

	public String getCustomer(String s);

	public String createOrder(String s, PlaceOrder p);

	public String showOrder(String s);

	public String cancelOrder(String s, String s1);

	public String getOrderDetails(String s, String s1);
	
}
