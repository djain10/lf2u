package cs445.iit.edu.models;

public class CustomerDetails {
	
	
	
	private String cid;
	private String name;
	private String street;
    private String zip;
    private String phone;
    private String email;
    

    public String getZip ()
    {
        return zip;
    }
    public CustomerDetails()
	{
		this.cid=Idgen.cid();
	}
    public CustomerDetails(String name, String phone,String street,String email, String zip) {
		super();
		this.cid=Idgen.cid();
		this.name = name;
		this.street = street;
		this.zip = zip;
		this.phone = phone;
		this.email = email;
	}
	public void setcid(String s)
    {
    	this.cid=s;
    }
    public String getcid()
    {
    	return this.cid;
    }
    
  
	public void setZip (String zip)
    {
        this.zip = zip;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getStreet ()
    {
        return street;
    }

    public void setStreet (String street)
    {
        this.street = street;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

}
