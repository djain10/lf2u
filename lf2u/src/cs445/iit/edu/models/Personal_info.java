package cs445.iit.edu.models;

public class Personal_info {
	private String phone;

    private String email;

    private String name;

    public Personal_info(String name, String phone, String email ) {
		super();
		this.phone = phone;
		this.email = email;
		this.name = name;
	}

	public Personal_info() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

}
