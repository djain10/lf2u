package cs445.iit.edu.models;

import java.util.List;

public class Report1 {
	private int frid;

    private String name;

    private List<Orders> orders;

    public int getFrid ()
    {
        return frid;
    }

    public void setFrid (int frid)
    {
        this.frid = frid;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public List<Orders> getOrders ()
    {
        return orders;
    }

    public void setOrders (List<Orders> orders)
    {
        this.orders = orders;
    }
}
