package cs445.iit.edu.models;

public class Farm_info {
	public Farm_info() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String fid;
    private String name;
	private String address;
    private String phone;
    private String web;
    
    public String getfid()
    {
        return fid;
    }

    public Farm_info(String name, String phone, String web,String address) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.web = web;
	}

	public void setfid(String fid)
    {
        this.fid = fid;
    
    }
    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getWeb ()
    {
        return web;
    }

    public void setWeb (String web)
    {
        this.web = web;
    }

}
