package cs445.iit.edu.models;

public class Order_detail {
	private String fspid;
	 private double amount;
	 public String getFspid ()
	    {
	        return fspid;
	    }

	    public void setFspid (String fspid)
	    {
	        this.fspid = fspid;
	    }
	    public double getamount ()
	    {
	        return amount;
	    }

	    public Order_detail(String fspid, double amount) {
			super();
			this.fspid = fspid;
			this.amount = amount;
		}

		public Order_detail() {
			super();
			// TODO Auto-generated constructor stub
		}

		

		public void setamount (double line_item_total)
	    {
	        this.amount = line_item_total;
	    }
}
