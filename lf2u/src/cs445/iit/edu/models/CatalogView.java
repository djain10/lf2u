package cs445.iit.edu.models;
public class CatalogView {
	private String name;

    private String gcpid;
    public CatalogView()
    {
    	this.gcpid=Idgen.gcpid();
    }

    public CatalogView(String name) {
		super();
		this.name = name;
		
	}

	public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }
    public void setgcpid(String s)
    {
    	this.gcpid=s;
    }

    public String getGcpid ()
    {
        return this.gcpid;
    }

   

}
