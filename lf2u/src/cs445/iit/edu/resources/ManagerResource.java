package cs445.iit.edu.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;

import cs445.iit.edu.BoundaryInterfaces.ManagerBoundaryInterface;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.services.ManagerService;

/**
=========================================================================================================================================================================
 * <h1>Manager Resource handle all the working of managers well as products.All the resources that are generated are in JSON format.</h1>
 * @author Divyank
 * @since Since:2016
=========================================================================================================================================================================
 **/



@Path("/managers")

public class ManagerResource {

	ManagerBoundaryInterface mbi = new ManagerService();
	Gson gson = new Gson();

	public StringBuilder ExtractString(InputStream incomingData) {
		StringBuilder jsonInString = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				jsonInString.append(line);
			}
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		return jsonInString;
	}

	@Path("/catalog")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response catalog() {
		String out;
		out=mbi.haveCatalog();
		if(out.equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else
		{
			return Response.status(200).entity(out).build();
		}
		
	}
	
	@Path("/catalog")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCatalog(InputStream incomingData) {
		String out; 
		StringBuilder out1 = ExtractString(incomingData);
		CatalogView use1;
		// mapping the value	
		 
		out = mbi.increCatalog(new Gson().fromJson(out1.toString(), CatalogView.class));
		if (out == "[]") {
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}

		return Response.status(201).entity(out).build();
	}

	@Path("/catalog/{gcpid}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response UpdateCatalog(InputStream incomingData, @PathParam("gcpid") String gcpid, @Context UriInfo i)
			throws JsonParseException, JsonMappingException, IOException {

		StringBuilder b;
		b = ExtractString(incomingData);
		boolean bol =mbi.updateCatalog(gcpid, new Gson().fromJson(b.toString(), CatalogView.class));
		if (bol == false) {
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		} else {
			UriBuilder builder = i.getAbsolutePathBuilder();
			builder.path(gcpid);
			return Response.created(builder.build()).build();
		}

	}

	@Path("/accounts")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response showAccount() {
		String i= mbi.getmlist();
		return Response.status(200).entity(i).build();

	}

	@Path("/accounts/{mid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response accountByID(@PathParam("mid") int mid) {
		String output;
	    output=mbi.getmanager(mid);
	    
		if (output.equals("[]")) {
			return Response.status(Response.Status.NOT_FOUND).entity("")
					.build();
		} else {
			return Response.status(200).entity(output).build();
		}
	}
	

	@Path("/reports")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReportList()

	{	String i=mbi.getreplist();
		return Response.status(200).entity(i).build();

	}

	@Path("/reports/{mrid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReportList2(@PathParam("mrid") int mrid, @QueryParam("start_date") String st1,
			@QueryParam("end_date") String st2)

	{
		String output;

		if (mrid == 1 || mrid == 2) {
			output = mbi.getManagerReport1(mrid, st1, st2);
		} else if (mrid == 3 || mrid == 4 || mrid == 5) {
			output = mbi.getManagerReport2(mrid, st1, st2);
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}

		return Response.status(200).entity(output).build();
	}

}