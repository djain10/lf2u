package cs445.iit.edu.resources;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import cs445.iit.edu.BoundaryInterfaces.DeliveryBoundaryInterface;
import cs445.iit.edu.services.DeliveryService;
/**
=========================================================================================================================================================================
 * <h1>DeliveryResource handle all the working of delivery. All the resources that are generated are in JSON format.</h1>
 * @author Divyank
 * @since Since:2016
=========================================================================================================================================================================
 */

@Path("/delivery")
public class DeliveryResource {
	DeliveryBoundaryInterface use=new DeliveryService();
	
boolean a;

	@Path("/{oid}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response Deliverysta(@PathParam("oid")String oid ,@Context UriInfo i)
	{ 
		if(use.Delivery(oid)=="no")	
		{
			 return Response.status(Response.Status.NOT_FOUND).entity("Order not found for ID: " + oid).build();
		}
		else if(use.Delivery(oid)=="can")
		{
			return Response.status(Response.Status.NOT_FOUND).entity("Order is already cancelled ID: " + oid).build();
		}
		else if(use.Delivery(oid)=="[]")
		{
			return Response.status(Response.Status.NOT_FOUND).entity("invalid oder details").build();
		}
		else{

		UriBuilder builder = i.getAbsolutePathBuilder();
	       builder.path(oid);
		return Response.created(builder.build()).build();
		}
		}
		
		
}
