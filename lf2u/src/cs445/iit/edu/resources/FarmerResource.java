package cs445.iit.edu.resources;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cs445.iit.edu.BoundaryInterfaces.FarmerBoundaryInterface;
import cs445.iit.edu.models.FarmNameId;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.Productdetails;
import cs445.iit.edu.services.FarmerService;


/**
=========================================================================================================================================================================
 * <h1>FarmerResource handle all the working of farmer as well as products.All the resources that are generated are in JSON format.</h1>
 * @author Divyank
 * @since Since:2016
=========================================================================================================================================================================
 **/


@Path("/farmers")
public class FarmerResource {
	Gson gson=new Gson();
	public static List<FarmNameId> farmnameidList = new ArrayList<>();
	boolean bol;
	FarmerBoundaryInterface bif=new FarmerService();
	public StringBuilder ExtractString(InputStream incomingData)
	{
		StringBuilder jsonInString = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				jsonInString.append(line);
			}
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		return jsonInString;
	}
	public boolean checkNull(Object b) throws IllegalAccessException {
		
		boolean b1=true;
		Field[] f1=b.getClass().getDeclaredFields();
	    for (Field f : f1)
	    {
	    	f.setAccessible(true);
	        if (f.get(this) != null)
	            b1=false;
	    }
	    return b1;            
	}
	

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createfarm(InputStream incomingData) throws IllegalAccessException
	{
String out;
		
		
		out=bif.createFarmer(new Gson().fromJson(ExtractString(incomingData).toString(),FarmerDetails.class ));
		if(out=="[]")
		{
			return Response.status(Response.Status.NOT_FOUND).entity("no Json ").build();
		}else
		{
		return Response.status(201).entity(out).build();
		}
	}
	
	@Path("/{fid}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatefarmaccount(@PathParam("fid")String fid ,InputStream incomingData, @Context UriInfo i)
	{	boolean bol;
	
	StringBuilder b;
	b=ExtractString(incomingData);
	
	
	bol=bif.updateFarmer(fid,new Gson().fromJson(b.toString(), FarmerDetails.class));
		if(bol==false)
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		
	else{
		UriBuilder builder = i.getAbsolutePathBuilder();
	       builder.path(fid);
		return Response.created(builder.build()).build();
	}
	}
	
	
	@Path("/{fid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response showfarmdetails(@PathParam("fid")String fid)
	{ 
		String out;
	out=bif.getFarm(fid);
		if(bif.getFarm(fid).equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else
		{
			return Response.status(200).entity(bif.getFarm(fid)).build();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response showfarmzip(@QueryParam("zip")String zip)
	{
		/*if(zip==null)
		{
			return Response.status(Response.Status.NOT_FOUND).entity("No input zip code").build();
		}
		String out;
		out=bif.zip(zip);
		
		if(out.equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else
		{
			return Response.status(200).entity(out).build();
		}
		
	}*/
		boolean noparam = false;
		if (zip == null) {
			noparam = true;
		}
		if (noparam) {
			System.out.println("In get All farmers");
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			List<FarmerDetails> farmerByZip = new ArrayList<>();
			List<FarmNameId> fniList = new ArrayList<>();
			farmerByZip = bif.viewAllFarmers();
			for (int i = 0; i < farmerByZip.size(); i++) {
				FarmNameId fi = new FarmNameId();
				String farmname = farmerByZip.get(i).getPersonal_info().getName();

				String fid = null;
				for (int j = 0; j < farmnameidList.size(); j++) {
					if (farmname.equals(farmnameidList.get(j).getName())) {
						fid = farmnameidList.get(j).getFid();
					}
				}
				fi.setFid(fid);
				fi.setName(farmerByZip.get(i).getPersonal_info().getName());
				fniList.add(fi);
			}
			String s = gson.toJson(fniList);
			return Response.status(Response.Status.OK).entity(s).build();
		} else { 
			System.out.println("In get All farmers by zip");
			List<FarmerDetails> farmerByZip = new ArrayList<>();
			List<FarmNameId> fniList = new ArrayList<>();
			String out;
			out=bif.zip(zip);
			
			if(out.equals("[]"))
			{
				return Response.status(Response.Status.NOT_FOUND).entity("").build();
			}
			else
			{
				return Response.status(200).entity(out).build();
			}
		}
	}

	@Path("/{fid}/products")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response showproduct(@PathParam("fid")String fid)
	{
		String out;
		out=bif.productsList(fid);
		if(out.equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else
		{
			return Response.status(200).entity(out).build();
		}
	}
	@Path("/{fid}/products")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addproduct(@PathParam("fid")String fid,InputStream incomingData)
	{Productdetails prod;
		
		String out;
		StringBuilder b;
		b=ExtractString(incomingData);
		
		
		out=bif.createProd(fid,new Gson().fromJson(b.toString(), Productdetails.class));
		if(out.equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else
		{
			return Response.status(200).entity(out).build();
		}
		
	}
	
	@Path("/{fid}/products/{fspid}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response addprice(@PathParam("fid")String fid,@PathParam("fspid")String fspid,InputStream incomingData, @Context UriInfo i)
	{
		
		 StringBuilder b;
		 Productdetails pd;
		 b=ExtractString(incomingData);	 
			
		bol=bif.updateProductInfo(fid,fspid,new Gson().fromJson(b.toString(), Productdetails.class));
		if(bol==false)
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		
	else{
		UriBuilder builder = i.getAbsolutePathBuilder();
	       builder.path("fid/fspid");
		return Response.created(builder.build()).build();
	}
		
		
	}
	
	@Path("/{fid}/products/{fspid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getproddetails(@PathParam("fid")String fid,@PathParam("fspid")String fspid)
	{
		String out;
		out=bif.getProductDetails(fid,fspid);
		if(out.equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else
		{
			return Response.status(200).entity(out).build();
		}
		
	}
	@Path("/{fid}/reports")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getreportlist(@PathParam("fid")String fid)
	
	{
		String out;
		out=bif.getReportList();
		return Response.status(200).entity(out).build();
	}
	@Path("/{fid}/reports/{frid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getreportlistda(@PathParam("fid")String fid,@PathParam("frid")int frid,@QueryParam("start_date") String st1,@QueryParam("end_date") String st2)
	
	{ String out;
		if(frid==701||frid==702)
		{
			out=bif.getReport(fid,frid);
			return Response.status(200).entity(out).build();
		}
		if(frid==(703)||frid==704)
		{
			if(st1==null||st2==null)
			{	
				out=bif.getReport1(fid,frid);
				if(out.equals("[]"))
				{
					return Response.status(Response.Status.NOT_FOUND).entity("No Report records found").build();
				}
				return Response.status(200).entity(out).build();
			}
			out=bif.getReport1(fid,frid,st1,st2);
			if(out.equals("[]"))
			{
				return Response.status(Response.Status.NOT_FOUND).entity("No Report records found").build();
			}
			return Response.status(200).entity(out).build();
		}
		
			return Response.status(Response.Status.NOT_FOUND).entity("No Report records found").build();
		
		
	}
	
@Path("/{fid}/delivery_charge")
@POST
@Consumes(MediaType.APPLICATION_JSON)
public Response setdeleiverycharges(@PathParam("fid")String fid,InputStream incomingData, @Context UriInfo i)
{
	
	
	StringBuilder b;
	
	cs445.iit.edu.models.Delivery d1; 
	b=ExtractString(incomingData);
	 
		d1=gson.fromJson(b.toString(), cs445.iit.edu.models.Delivery.class);
    if(bif.deliveryCharge(fid,d1)==false)
	{
		return Response.status(Response.Status.NOT_FOUND).entity("").build();
	}
	
else{
	UriBuilder builder = i.getAbsolutePathBuilder();
      builder.path(fid);
	return Response.created(builder.build()).build();
}
}
@Path("/{fid}/delivery_charge")
@GET
@Produces(MediaType.APPLICATION_JSON)
public Response setdeleiverycharges(@PathParam("fid")String fid)
{

	String out;
	out=bif.getDeliveryCharges(fid);
	if(out.equals("[]"))
	{
		return Response.status(Response.Status.NOT_FOUND).entity("").build();
	}
	else
	{
		return Response.status(200).entity(out).build();
	}
	
}
}
	

