package cs445.iit.edu.resources;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;

import cs445.iit.edu.BoundaryInterfaces.CustomerBoundaryInterface;
import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.PlaceOrder;
import cs445.iit.edu.services.CustomerService;


/**
=========================================================================================================================================================================
 * <h1>CustomerResource handle all the working of customer as well as order.All the resources that are generated are in JSON format.</h1>
 * @author Divyank
 * @since Since:2016
=========================================================================================================================================================================
 **/

@Path("/customers")
public class CustomerResource {
	CustomerBoundaryInterface cbi=new CustomerService();
	String ret;
	public StringBuilder ExtractString(InputStream incomingData)
	{
		
		StringBuilder jsonInString = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				jsonInString.append(line);
			}
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		return jsonInString;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createaccount(InputStream incomingData)
	{
		CustomerDetails h;
		
		StringBuilder b=ExtractString(incomingData);
		Gson g=new Gson();
		h=g.fromJson(b.toString(), CustomerDetails.class);
		ret=cbi.createAccount(h);
		
		
		return Response.status(201).entity(ret).build();
		
	}
	@Path("/{cid}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateaccount(@PathParam("cid")String cid,InputStream incomingData,@Context UriInfo i)
	{
		
		boolean a;
		
		StringBuilder b=ExtractString(incomingData);
		Gson gson = new Gson();
		
		a=cbi.update(cid,gson.fromJson(b.toString(),CustomerDetails.class));
		if(a==false)
		{
			 return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else
		{
			UriBuilder builder = i.getAbsolutePathBuilder();
		       builder.path(cid);
			return Response.created(builder.build()).build();
		}
		
	}
	@Path("/{cid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response Getcustomeraccount(@PathParam("cid")String cid)
	{
		
		ret=cbi.getCustomer(cid);
		if(ret.equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("" + cid).build();
		}
		else
		{
			return Response.status(200).entity(ret).build();
		}
		
	}
	
	@Path("/{cid}/orders")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createOrder(@PathParam("cid")String cid,InputStream incomingData)
	{
		
		StringBuilder b;
		b=ExtractString(incomingData);
		 PlaceOrder plc;
		Gson gson=new Gson();
	      plc=gson.fromJson(b.toString(), PlaceOrder.class);
		ret=cbi.createOrder(cid,plc);
		
		if(ret.equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else if(ret=="no zip")
		{
			return Response.status(422).entity("invalid zip entry").build();
		}
		else
		{
			return Response.status(200).entity(ret).build();	
		}
	}
	
	@Path("/{cid}/orders")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getorders(@PathParam("cid")String cid)
	{
		
		String output;
		output=cbi.showOrder(cid);
		if(output.equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("[]").build();
		}
		else
		{
			return Response.status(200).entity(output).build();
		}
	}
	
	@Path("/{cid}/orders/{oid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getorderswithid(@PathParam("cid")String cid,@PathParam("oid")String oid)
	{  
		
		ret=cbi.getOrderDetails(cid,oid);
		if(cbi.getOrderDetails(cid,oid).equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else
		{
			return Response.status(200).entity(ret).build();
		}
		
	
	}
	
	@Path("/{cid}/orders/{oid}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancel(@PathParam("cid")String cid,@PathParam("oid")String oid)
	{	
		if(cbi.cancelOrder(cid,oid).equals("[]"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		}
		else if (ret.equals("nada"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();	
		}
		else if(ret.equals("done"))
		{
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
			
		}
		else
			{
			return Response.status(200).entity(cbi.cancelOrder(cid,oid)).build();
		}
		
	}
	
	

	
}
