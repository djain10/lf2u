package cs445.iit.edu.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cs445.iit.edu.BoundaryInterfaces.SearchBoundaryInterface;
import cs445.iit.edu.services.SearchService;

@Path("search")
public class SearchResource {

	SearchBoundaryInterface sbi = new SearchService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getdetails(@QueryParam("topic") String topic, @QueryParam("key") String key) {
		if (topic == null || key == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("").build();

		}

		if (sbi.search(topic, key).equals("wrong")) {
			return Response.status(Response.Status.NOT_FOUND).entity("" + topic).build();
		}

		if (sbi.search(topic, key).equals("[]")) {
			return Response.status(Response.Status.NOT_FOUND).entity("").build();
		} else {
			return Response.status(200).entity(sbi.search(topic, key)).build();
		}

	}

}
