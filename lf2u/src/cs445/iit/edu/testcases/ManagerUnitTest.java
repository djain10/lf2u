package cs445.iit.edu.testcases;

import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.Farm_info;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.Idgen;
import cs445.iit.edu.models.Order_detail;
import cs445.iit.edu.models.Personal_info;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.models.PlaceOrder;
import cs445.iit.edu.services.CustomerService;
import cs445.iit.edu.services.DeliveryService;
import cs445.iit.edu.services.FarmerService;
import cs445.iit.edu.services.ManagerService;
import cs445.iit.edu.services.SearchService;

public class ManagerUnitTest {
    CustomerService s;
    DeliveryService d;
    FarmerService f;
    ManagerService m;
    SearchService sr;
    CustomerDetails c;
    Idgen i;
    @Before
    public void setup()
    {
        s=new CustomerService();
        d=new DeliveryService();
        f=new FarmerService();
         m=new ManagerService();
         sr=new SearchService();
         c=new CustomerDetails();
         i=new Idgen();
            
         
    }
    @After
    public void finish()
    {
        s.trash();
        f.trash();
        m.trash();
        i.trash();
        
        
    }

    @Test
    public void testGetreplist() {
         String g=m.getreplist();
         boolean a1= Pattern.compile(Pattern.quote("mrid"), Pattern.CASE_INSENSITIVE).matcher(g).find();
            assertEquals(a1,true);
    }

    @Test
    public void testGetmlist() {
        String g=m.getmlist();
         boolean a1= Pattern.compile(Pattern.quote("manager"), Pattern.CASE_INSENSITIVE).matcher(g).find();
            assertEquals(a1,true);
        
    }

    @Test
    public void testGetmanager() {
String g=m.getmanager(1);
boolean a1= Pattern.compile(Pattern.quote("manager1"), Pattern.CASE_INSENSITIVE).matcher(g).find();
assertEquals(a1,true);
        
    }

    @Test
    public void testGetreportt1() {
      FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
        String[] fp={"60010", "60011"};
        
        fr.setDelivers_to(fp);
        fr.setFarm_info(fi);
        fr.setPersonal_info(pl);
        f.createFarmer(fr);
        c.setName("sriram");
        c.setPhone("9003203629");
        c.setStreet("woodie drive");
        c.setEmail("sriram@gmail.com");
        c.setZip("60010");
        s.createAccount(c);
        PlaceOrder p=new PlaceOrder();
        Order_detail o=new Order_detail();
        Order_detail[] oa=new Order_detail[1];
        p.setDelivery_note("successu");
        p.setFid("1");
        o.setamount(2);
        o.setFspid("1");
        oa[0]=o;
        p.setOrder_detail(oa);
        s.createOrder("1", p);
        String g=m.getManagerReport1(1,null,null);
        boolean a1= Pattern.compile(Pattern.quote("1"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
        
    }

    @Test
    public void testGetreportt2() {
      FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
        String[] fp={"60010", "60011"};
        
        fr.setDelivers_to(fp);
        fr.setFarm_info(fi);
        fr.setPersonal_info(pl);
       f.createFarmer(fr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		 s.createAccount(c);
        PlaceOrder p=new PlaceOrder();
        Order_detail o=new Order_detail();
        Order_detail[] oa=new Order_detail[1];
        p.setDelivery_note("successu");
        p.setFid("1");
        o.setamount(2);
        o.setFspid("1");
        oa[0]=o;
        p.setOrder_detail(oa);
        s.createOrder("1", p);
        String g=m.getManagerReport1(2,null,null);
        boolean a1= Pattern.compile(Pattern.quote("0"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
    }

   /* @Test
    public void testGetreportt3() {
      Farmerdata fr=new Farmerdata();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		m.increCatalog(ca);
        String[] fp={"60010", "60011"};
        
        fr.setDelivers_to(fp);
        fr.setFarm_info(fi);
        fr.setPersonal_info(pl);
       f.createFarmer(fr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		 s.createAccount(c);
        place_order p=new place_order();
        Order_detail o=new Order_detail();
        Order_detail[] oa=new Order_detail[1];
        p.setDelivery_note("successu");
        p.setFid("1");
        o.setamount(2);
        o.setFspid("1");
        oa[0]=o;
        p.setOrder_detail(oa);
        s.createOrder("1", p);
        String g=m.getManagerReport1(2,"20161110","20161122");
        boolean a1= Pattern.compile(Pattern.quote("1"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
    }
    */
    @Test
    public void testGetreportt4() {
      FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
        String[] fp={"60010", "60011"};
        
        fr.setDelivers_to(fp);
        fr.setFarm_info(fi);
        fr.setPersonal_info(pl);
       f.createFarmer(fr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		 s.createAccount(c);
        PlaceOrder p=new PlaceOrder();
        Order_detail o=new Order_detail();
        Order_detail[] oa=new Order_detail[1];
        p.setDelivery_note("successul");
        p.setFid("1");
        o.setamount(2);
        o.setFspid("1");
        oa[0]=o;
        p.setOrder_detail(oa);
        s.createOrder("1", p);
        String g=m.getManagerReport2(3,null,null);
        boolean a1= Pattern.compile(Pattern.quote("0"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
    }
    @Test
    public void testGetreportt5() {
      FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
        String[] fp={"60010", "60011"};
        
        fr.setDelivers_to(fp);
        fr.setFarm_info(fi);
        fr.setPersonal_info(pl);
       f.createFarmer(fr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		 s.createAccount(c);
        PlaceOrder p=new PlaceOrder();
        Order_detail o=new Order_detail();
        Order_detail[] oa=new Order_detail[1];
        p.setDelivery_note("successu");
        p.setFid("1");
        o.setamount(2);
        o.setFspid("1");
        oa[0]=o;
        p.setOrder_detail(oa);
        s.createOrder("1", p);
        String g=m.getManagerReport2(4,null,null);
        boolean a1= Pattern.compile(Pattern.quote("0"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
    }
    @Test
    public void testGetreportt6() {
      FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
        String[] fp={"60010", "60011"};
        
        fr.setDelivers_to(fp);
        fr.setFarm_info(fi);
        fr.setPersonal_info(pl);
       f.createFarmer(fr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		 s.createAccount(c);
        PlaceOrder p=new PlaceOrder();
        Order_detail o=new Order_detail();
        Order_detail[] oa=new Order_detail[1];
        p.setDelivery_note("successu");
        p.setFid("1");
        o.setamount(2);
        o.setFspid("1");
        oa[0]=o;
        p.setOrder_detail(oa);
        s.createOrder("1", p);
        String g=m.getManagerReport2(4,"20161111","20161122");
        boolean a1= Pattern.compile(Pattern.quote("1"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
    }
    @Test
    public void testAddcat() {
        CatalogView ca=new CatalogView();
        String g=m.increCatalog(ca);
        boolean a1= Pattern.compile(Pattern.quote("1"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
    }

    @Test
    public void testUpdate() {
        CatalogView ca=new CatalogView();
        ca.setName("potato");
        m.increCatalog(ca);
        CatalogView ca1=new CatalogView();
        ca1.setName("tomato");
        boolean g=m.updateCatalog("1", ca1);
        assertEquals(g,true);
        
    }

}