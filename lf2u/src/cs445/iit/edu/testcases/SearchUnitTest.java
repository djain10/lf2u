package cs445.iit.edu.testcases;

import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.Farm_info;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.Idgen;
import cs445.iit.edu.models.Order_detail;
import cs445.iit.edu.models.Personal_info;
import cs445.iit.edu.models.Productdetails;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.models.PlaceOrder;
import cs445.iit.edu.services.CustomerService;
import cs445.iit.edu.services.DeliveryService;
import cs445.iit.edu.services.FarmerService;
import cs445.iit.edu.services.ManagerService;
import cs445.iit.edu.services.SearchService;

public class SearchUnitTest {
	CustomerService s;
	DeliveryService d;
	FarmerService f;
	ManagerService m;
	SearchService sr;
	CustomerDetails c;
	Idgen i;
	@Before
	public void setup()
	{
		s=new CustomerService();
		d=new DeliveryService();
		f=new FarmerService();
		 m=new ManagerService();
		 sr=new SearchService();
		 c=new CustomerDetails();
		 i=new Idgen();
			
		 
	}
	@After
	public void finish()
	{
		s.trash();
		f.trash();
		m.trash();
		i.trash();
		
		
	}
	

	@Test
	public void testSearchf() {
		 FarmerDetails fr1=new FarmerDetails();
			Farm_info fi1=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
			Personal_info pl1=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
			CatalogView ca=new CatalogView("potato");
		String[] fp={"60010", "60011"};
		fr1.setDelivers_to(fp);
		fr1.setFarm_info(fi1);
		fr1.setPersonal_info(pl1);
		f.createFarmer(fr1);
		String g=sr.Searchf("john");
		 boolean a1= Pattern.compile(Pattern.quote("john"), Pattern.CASE_INSENSITIVE).matcher(g).find();
	        assertEquals(a1,true);
		
	}

	@Test
	public void testSearchc() {

		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		String g=sr.Searchc("asd");
        boolean a1= Pattern.compile(Pattern.quote("asdfgh"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
	}

	@Test
	public void testSearcho() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		Productdetails pr= new Productdetails("workes","20161110","",0.10d,"1","","lb");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("working");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		String g=sr.Searcho("john");
        boolean a1= Pattern.compile(Pattern.quote("working"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
	}

	
}
