package cs445.iit.edu.testcases;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   FarmerUnitTest.class,
   CustomerUnitTest.class,
   SearchUnitTest.class,DeliveryUnitTest.class,ManagerUnitTest.class
})
  	
public class Testcaseclass {

}
