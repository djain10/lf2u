package cs445.iit.edu.testcases;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.Farm_info;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.Idgen;
import cs445.iit.edu.models.Order_detail;
import cs445.iit.edu.models.Personal_info;
import cs445.iit.edu.models.Productdetails;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.models.PlaceOrder;
import cs445.iit.edu.services.CustomerService;
import cs445.iit.edu.services.DeliveryService;
import cs445.iit.edu.services.FarmerService;
import cs445.iit.edu.services.ManagerService;
import cs445.iit.edu.services.SearchService;


public class DeliveryUnitTest {
	CustomerService s;
	DeliveryService d;
	FarmerService f;
	ManagerService m;
	SearchService sr;
	CustomerDetails c;
	Idgen i;
	@Before
	public void setup()
	{
		s=new CustomerService();
		d=new DeliveryService();
		f=new FarmerService();
		 m=new ManagerService();
		 sr=new SearchService();
		 c=new CustomerDetails();
		 i=new Idgen();
			
		 
	}
	@After
	public void finish()
	{
		s.trash();
		f.trash();
		m.trash();
		i.trash();
		
		
	}

	@Test
	public void testDelivery() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info();
		Personal_info pl=new Personal_info();
		
		
		
		CatalogView ca=new CatalogView();
		ca.setName("potato");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fi.setName("boss");
		fi.setPhone("8089842432");
		fi.setWeb("bhasheyam@gmail.com");
		fi.setAddress("napervile");
		pl.setName("bhasheyam");
		pl.setPhone("873982738");
		pl.setEmail("bhasheyam@gmai.com");
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		Productdetails pr= new Productdetails();
		pr.setStart_date("20161110");
		pr.setEnd_date("");
		pr.setNote("sucess");
		pr.setPrice(0.10d);
		pr.setGcpid("1");
		pr.setImage("");
		pr.setProduct_unit("lb");
		c.setName("sriram");
		c.setPhone("9003203629");
		c.setStreet("woodie drive");
		c.setEmail("sriram@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
	String g=d.Delivery("1");
	assertEquals(g,"done");
	
		
	}

}
