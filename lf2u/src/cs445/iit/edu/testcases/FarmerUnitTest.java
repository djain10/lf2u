package cs445.iit.edu.testcases;

import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.Delivery;
import cs445.iit.edu.models.Farm_info;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.Idgen;
import cs445.iit.edu.models.Order_detail;
import cs445.iit.edu.models.Personal_info;
import cs445.iit.edu.models.Productdetails;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.models.PlaceOrder;
import cs445.iit.edu.services.CustomerService;
import cs445.iit.edu.services.DeliveryService;
import cs445.iit.edu.services.FarmerService;
import cs445.iit.edu.services.ManagerService;
import cs445.iit.edu.services.SearchService;


public class FarmerUnitTest {
	CustomerService s;
	DeliveryService d;
	FarmerService f,f1;
	ManagerService m;
	SearchService sr;
	CustomerDetails c;
	Idgen i;
	@Before
	public void setup()
	{
		s=new CustomerService();
		d=new DeliveryService();
		f=new FarmerService();
		f1=new FarmerService();
		 m=new ManagerService();
		 sr=new SearchService();
		 c=new CustomerDetails();
		 i=new Idgen();
			
		 
	}
	@After
	public void finish()
	{
		s.trash();
		f.trash();
		m.trash();
		i.trash();
		
		
	}

	@Test
	public void testgetreportlist() {
		String g=f.getReportList();
		boolean a1= Pattern.compile(Pattern.quote("701"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
	}
	
	@Test
	public void testCreate() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		int i=f.getFarmList().size();
		assertEquals(i,1);
		
	}

	@Test
	public void testUpdate() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		
		
		
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		
		FarmerDetails fr1=new FarmerDetails();
		Farm_info fi1=new Farm_info("john 2","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl1=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		String[] fp1={"60010", "60011"};
		fr1.setDelivers_to(fp1);
		fr1.setFarm_info(fi1);
		fr1.setPersonal_info(pl1);
		
		
		f.updateFarmer("1", fr1);
		int i=f.getFarmList().size();
		assertEquals(i,1);
		
	}

	@Test
	public void testGetfarm() {
		FarmerDetails fr=new FarmerDetails();
		
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi); 
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		
		String g=f.getFarm("1");
		 boolean a1= Pattern.compile(Pattern.quote("john"), Pattern.CASE_INSENSITIVE).matcher(g).find();
	        assertEquals(a1,true);
	}

	@Test
	public void testZip() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		String g=f.zip("60010");
		 boolean a1= Pattern.compile(Pattern.quote("john"), Pattern.CASE_INSENSITIVE).matcher(g).find();
	        assertEquals(a1,true);
	}

	@Test
	public void testProductslist() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
		ca.setName("potato");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		f.createProd("1", pr);
		String g=f.productsList("1");
		boolean a1= Pattern.compile(Pattern.quote("20161110"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
		
		
	}

	@Test
	public void testCreateprod() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
		
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fi.setName("boss");
		fi.setPhone("8089842432");
		fi.setWeb("bhasheyam@gmail.com");
		fi.setAddress("napervile");
		pl.setName("bhasheyam");
		pl.setPhone("873982738");
		pl.setEmail("bhasheyam@gmai.com");
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
	
		String g=f.createProd("1", pr);
		 boolean a1= Pattern.compile(Pattern.quote(""), Pattern.CASE_INSENSITIVE).matcher(g).find();
	        assertEquals(a1,true);
		
	}

	@Test
	public void testUpdateproductinfo() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		Productdetails pr= new Productdetails();
		Productdetails pr1= new Productdetails();
		pr.setStart_date("20161110");
		pr.setEnd_date("");
		pr.setNote("sucess");
		pr.setPrice(0.10d);
		pr.setGcpid("1");
		pr.setImage("");
		//pr.setPrice(5.0);
		pr.setProduct_unit("lb");
		f.createProd("1", pr);
		boolean a=f.updateProductInfo("1", "1", pr1);
		assertEquals(a,true);
	}

	@Test
	public void testGetproductdetails() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		Productdetails pr= new Productdetails();
		Productdetails pr1= new Productdetails();
		pr.setStart_date("20161110");
		pr.setEnd_date("");
		pr.setNote("sucess");
		pr.setPrice(0.10d);
		pr.setGcpid("1");
		pr.setImage("");
		pr.setPrice(5.0);
		pr.setProduct_unit("lb");
		f.createProd("1", pr);
		String g=f.getProductDetails("1", "1");
		boolean a1= Pattern.compile(Pattern.quote("sucess"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
		
		
	}

	@Test
	public void testGetreportlist() {

		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
			m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		
		f.createProd("1", pr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		String g=f.getReport("1", 702);
		boolean a1= Pattern.compile(Pattern.quote("702"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
	}

	@Test
	public void testGetreportlist1() {

		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		String g=f.getReport("1", 703);
		boolean a1= Pattern.compile(Pattern.quote("0"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
	}
	@Test
	public void testreporttype703()
	{
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		String g=f.getReport1("1", 703,"20161110","20161121");
		boolean a1= Pattern.compile(Pattern.quote("0"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
		
	}
	/*@Test
	public void testGetreportlist2() {

		Farmerdata fr=new Farmerdata();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);		
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		place_order p=new place_order();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		d.Delivery("1");
		String g=f.getReport1("1", 704, "20161110", "20161125");
		 boolean a1= Pattern.compile(Pattern.quote("1"), Pattern.CASE_INSENSITIVE).matcher(g).find();
	        assertEquals(a1,false);
		
		
	}
*/	
	@Test
	public void testDeliverycharge() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		Delivery dn=new Delivery();
		dn.setcharge(5.0);
		boolean a=f.deliveryCharge("1", dn);
		assertEquals(a,true);
		
	}

	@Test
	public void testGetdeliverycharges() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		Delivery dn=new Delivery();
		dn.setcharge(5.0);
		f.deliveryCharge("1", dn);
		String g=f.getDeliveryCharges("1");
		 boolean a1= Pattern.compile(Pattern.quote("5.0"), Pattern.CASE_INSENSITIVE).matcher(g).find();
	        assertEquals(a1,true);
		
	}

}
