package cs445.iit.edu.testcases;

import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.Farm_info;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.Idgen;
import cs445.iit.edu.models.Order_detail;
import cs445.iit.edu.models.Personal_info;
import cs445.iit.edu.models.Productdetails;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.models.PlaceOrder;
import cs445.iit.edu.services.CustomerService;
import cs445.iit.edu.services.FarmerService;
import cs445.iit.edu.services.ManagerService;
import cs445.iit.edu.services.SearchService;
import cs445.iit.edu.services.DeliveryService;

public class CustomerUnitTest {
	CustomerService s;
	DeliveryService d;
	FarmerService f;
	ManagerService m;
	SearchService sr;
	CustomerDetails c;
	Idgen i;
	@Before
	public void setup()
	{
		s=new CustomerService();
		d=new DeliveryService();
		f=new FarmerService();
		 m=new ManagerService();
		 sr=new SearchService();
		 c=new CustomerDetails("asdfgh","9425060729","hercae asdfg","asdfgh@gmail.com","60616");
		 i=new Idgen();
			
		 
	}
	@After
	public void finish() 
	{
		s.trash();
		f.trash();
		m.trash();
		i.trash();
		
		
	}

	@Test
	public void testCreateaccount() {
		
		s.createAccount(c);
		int i=s.getcustomer().size();
		assertEquals(1,i);
		
	}

	@Test
	public void testUpdate() {
		
		
		CustomerDetails c1=new CustomerDetails("changed","9425060729","hercae asdfg","asdfgh@gmail.com","60616");
		
		s.createAccount(c);
		s.update("51", c1);
		int i=s.getcustomer().size();
		assertEquals(1,i);
		
	}

	@Test
	public void testGetcustomerString() {
		
		s.createAccount(c);
		String g=s.getCustomer("1");
        boolean a1= Pattern.compile(Pattern.quote("asdfgh"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
		
	}
	@Test
	public void testgetorder() {
		
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		String g=s.showOrder("1");
		
		
		boolean a1= Pattern.compile(Pattern.quote("open"), Pattern.CASE_INSENSITIVE).matcher(g).find();
        assertEquals(a1,true);
	}

	@Test
	public void testCreateorder() {
		
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1"); 
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		
		int i=s.getplaceorder().size();
		assertEquals(3,i);
	}

	@Test 
	public void testShoworder() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		String g=s.showOrder("1");
		 boolean a1= Pattern.compile(Pattern.quote("1"), Pattern.CASE_INSENSITIVE).matcher(g).find();
	        assertEquals(a1,true);
	}

	

	@Test
	public void testGetorderdetails() {
		FarmerDetails fr=new FarmerDetails();
		Farm_info fi=new Farm_info("john","9807654321","mayankjnn@gmail.com","indoreeee");
		Personal_info pl=new Personal_info("mayankjnn","903946746","mayankjnn@gmai.com");
		CatalogView ca=new CatalogView("potato");
		m.increCatalog(ca);
		String[] fp={"60010", "60011"};
		fr.setDelivers_to(fp);
		fr.setFarm_info(fi);
		fr.setPersonal_info(pl);
		f.createFarmer(fr);
		Productdetails pr= new Productdetails("sucess","20161110","",0.10d,"1","","lb");
		c.setName("asdfgh");
		c.setPhone("9425060729");
		c.setStreet("hercae asdfg");
		c.setEmail("asdfgh@gmail.com");
		c.setZip("60010");
		s.createAccount(c);
		PlaceOrder p=new PlaceOrder();
		Order_detail o=new Order_detail();
		Order_detail[] oa=new Order_detail[1];
		p.setDelivery_note("successu");
		p.setFid("1");
		o.setamount(2);
		o.setFspid("1");
		oa[0]=o;
		p.setOrder_detail(oa);
		s.createOrder("1", p);
		s.cancelOrder("1", "1");
		String g=s.getOrderDetails("1", "1");
		 boolean a1= Pattern.compile(Pattern.quote("cancelled"), Pattern.CASE_INSENSITIVE).matcher(g).find();
	        assertEquals(a1,true);
	}

}
