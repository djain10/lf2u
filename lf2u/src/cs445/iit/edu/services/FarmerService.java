package cs445.iit.edu.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cs445.iit.edu.BoundaryInterfaces.FarmerBoundaryInterface;
import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.Delivery;
import cs445.iit.edu.models.FarmerReport;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.OrderReport;
import cs445.iit.edu.models.Ordered_by;
import cs445.iit.edu.models.Productdetails;
import cs445.iit.edu.models.Productlist;
import cs445.iit.edu.models.Report;
import cs445.iit.edu.models.Report1;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.models.deliverysup;
import cs445.iit.edu.models.fid;
import cs445.iit.edu.models.fspid;
import cs445.iit.edu.models.Orderget;
import cs445.iit.edu.models.Orders;
import cs445.iit.edu.models.report3;
import cs445.iit.edu.models.zipfarm;

public class FarmerService implements FarmerBoundaryInterface {
	
public static List<FarmerDetails> farmdata=new ArrayList<FarmerDetails>();
public static List<Productdetails> proddet=new ArrayList<Productdetails>();
public static List<Productlist> prodl=new ArrayList<Productlist>();
public static List<Delivery>delivery =new ArrayList<Delivery>();
List<Productdetails> proddet2=new ArrayList<Productdetails>();
List<zipfarm> zipfarm=new ArrayList<zipfarm>();
String ret;
Gson gson = new GsonBuilder().setPrettyPrinting().create();
boolean bol = false;
public static List<FarmerDetails> getFarmList()
{
	return farmdata;
}
	@Override
	public String createFarmer(FarmerDetails f) {
		
		farmdata.add(f);
		fid fi=new fid();
		fi.set(f.getfid());	
		
		 return gson.toJson(fi);
		
		
	}
	@Override
	public boolean updateFarmer(String fid, FarmerDetails fmd) {
		
		
		try{
		for(FarmerDetails fmd2:farmdata)
		{
			if(fmd2.getfid().equals(fid))
			{
					fmd.setfid(fmd2.getfid());
				farmdata.set(farmdata.indexOf(fmd2),fmd);
				bol=true;
			}
		}
		}
		catch(Exception e)
		{
			return false;
		}
		
		return bol;
		
	}
	public List<FarmerDetails> viewAllFarmers() {
		return (farmdata);
	}

	@Override
	public String getFarm(String fid) {
		
		for(FarmerDetails fmd:farmdata)
		{
			if(fmd.getfid().equals(fid))
			{
					 
			 return gson.toJson(fmd);
			}
		}
	return "[]";
		
	}
	public static List<Delivery> getDeliveryList()
	{
		return delivery;
	}
	@Override
	public String zip(String zi) throws NullPointerException {
	
	
	for(FarmerDetails e1:farmdata)
	{
		String[] z=new String[100];
		z=e1.getDelivers_to();
		try{
			
		
		for(String d:z)
			{
				if(zi.equals(d))
				{
					zipfarm zip=new zipfarm();
					zip.setfid(e1.getfid());
					zip.setname(e1.getFarm_info().getName());
					zipfarm.add(zip);
					
				}
			}	
		
	}
	catch(Exception e)
	{
		return "[]";
	}
	}
		 
		 return gson.toJson(zipfarm);
		
	}
	public static List<Productdetails> getProductList()
	{
		return proddet;
	}

	@Override
	public String productsList(String s) {
		
		for(Productlist plist:prodl)
		{
			
			if(plist.getid().equals(s))
			{
				proddet2.add(plist.getprod());
			}
		}
			
		return gson.toJson(proddet2);
	}
	@Override
	public String createProd(String strang, Productdetails  prod) {
		
		List<CatalogView> cat=new ArrayList<CatalogView>();
		String c=prod.getfspid();String out;
		cat=ManagerService.getlist();
		for(CatalogView catmag:cat)
		{
			
			if(prod.getGcpid().equals(catmag.getGcpid()))
			{
				bol=true;
			}
		}
		if(bol=true)
		{
			
		proddet.add(prod);
		Productlist prodlet=new Productlist();
		prodlet.setid(strang);
		prodlet.setpid(prod.getfspid());
		prodlet.setprod(prod);
		prodl.add(prodlet);
				
		fspid fs=new fspid();
		fs.setfspid(c);
		Gson f=new GsonBuilder().setPrettyPrinting().create();
		out=f.toJson(fs);
		return out;
		}
	return "[]";	
	}

public static List<Productlist> getproductlist2()
{
	return prodl;
}

	@Override
	public boolean updateProductInfo(String s, String s1, Productdetails pd) {
		
		
			for(Productlist prod:prodl)
		{
			
			if(prod.getid().equals(s))
			{
		
		for(Productdetails pdl:proddet)
		{			
			if(pdl.getfspid().equals(s1))
			{
				
				if(pd.getNote()!=null)
				{
					pdl.setNote(pd.getNote());
				}
				
				if(pd.getPrice()!=0.0d)
				{
					pdl.setPrice(pd.getPrice());
				}
				
				if(pd.getStart_date()!=null)
				{
					pdl.setStart_date(pd.getStart_date());
				}
								if(pd.getEnd_date()!=null)
				{
					pdl.setEnd_date(pd.getEnd_date());
				}
				
				if(pd.getProduct_unit()!=null)
				{
					pdl.setProduct_unit(pd.getProduct_unit());
				}
				
				if(pd.getImage()!=null)
				{
					pdl.setImage(pd.getImage());
				} 
				for(Productlist h:prodl)
				{
										if(h.getpid().equals(s1))
					{
						h.setprod(pdl);
						bol=true;
					}
					
				}
			}
			
		}
		} 
		}
		return bol;
	}
	@Override
	public String getProductDetails(String strang, String strang1) {
		
	String k,k1;
	
		for(Productlist h:prodl)
		{
			k=h.getpid();
			k1=h.getid();
			if(k1.equals(strang) && k.equals(strang1))
			{
							
				return gson.toJson(h.getprod());
				
			}
		
	}
		return "[]";
	}
	@Override
	public String getReportList() {
		
		FarmerReport report=new FarmerReport();
		       
		return gson.toJson(report.getlist());
		
	}
	@Override
	public boolean deliveryCharge(String strang,Delivery deliv) {
		
		
		try{
		
		for(FarmerDetails r:farmdata)
		{
			if(strang.equals(r.getfid()))
	        {
				r.setdeliverycharges(deliv.getcharges());
				deliv.setfid(strang);
				delivery.add(deliv);
				bol=true;
	      }
		}
		}
		catch(Exception e)
		{
			return false;
		}
		
    	
		return bol;
	}
	@Override
	public String getDeliveryCharges(String strang) {
		
		deliverysup deliv=new deliverysup();
		for(Delivery j:delivery)
		{
						if(j.getid().equals(strang))
			{
	           deliv.setcharge(j.getcharges());
	           
	           return gson.toJson(deliv);
	           
			}
		
		}
		
		return"[]";
	}
	public  String getCurrentDate()
	{
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date dateobj = new Date();
		return df.format(dateobj);
	}
	
	
	public String getcid(String s)
	{ String ret="";
		List<Orderget> ord=new ArrayList<Orderget>();
		
		ord=CustomerService.getorderget();
		for(Orderget k:ord)
		{
			if(k.getoid().equals(s))
			{
				ret=k.getcid();
			}
		}
		return ret;
		
	}
	@Override
	public String getReport(String strang, int strang1) {
		String key,cid;
		List<Orders> ordl=new ArrayList<Orders>();
		List<OrderReport> orderep=new ArrayList<OrderReport>();
		List<CustomerDetails> custdet=new ArrayList<CustomerDetails>();
		orderep=CustomerService.getorderlist();
		custdet=CustomerService.getcustomer();
		Report1 rep1=new Report1();
		try{
		if(strang1==701)
		{
			rep1.setFrid(strang1);
			rep1.setName("Orders to deliver today");
			key=getCurrentDate();
			
		}
		else
		{
			rep1.setFrid(strang1);
			rep1.setName("Orders to deliver tomorrow");
			key=getNextDay();
			
		}
		
		
		for(OrderReport o:orderep)
		{
			Orders ord=new Orders();
			Ordered_by ord1=new Ordered_by();
			cid=getcid(o.getOid());
			
			if(o.getFarm_info().getfid().equals(strang)&& o.getPlanned_delivery_date().equals(key))
			{	
				
				
				ord.setDelivery_charge(o.getDelivery_charge());
				ord.setOrder_total(o.getOrder_total());
				ord.setProducts_total(o.getProducts_total());
				
				ord.setOrder_detail(o.getOrder_detail());
				ord.setoid(o.getOid());
				ord.setActual_delivery_date(o.getActual_delivery_date());
				ord.setOrder_date(o.getOrder_date());
				ord.setPlanned_delivery_date(o.getPlanned_delivery_date());
				ord.setNote(o.getDelivery_note());
				ord.setStatus(o.getStatus());
				for(CustomerDetails c:custdet)
				{
					
					if(c.getcid().equals(cid))
					{
						ord1.setName(c.getName());
						ord1.setEmail(c.getEmail());
						ord1.setPhone(c.getPhone());
						ord.setDelivery_address(c.getStreet()+" "+c.getZip());	
						ord.setOrdered_by(ord1);
				}
				}
			}
			if(ord.getOrder_total()==0.0d)
			{
				continue;
			}else{
			ordl.add(ord);
			}
			}
		rep1.setOrders(ordl);	
		}
	
	catch(Exception e)
	{
		return "[]";
	}
				 return gson.toJson(rep1);
		
	}
	public   String getNextDay()
	{
		
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date dateobj = new Date();
		Calendar cal = Calendar.getInstance();
	    cal.setTime(dateobj);
	    cal.add(Calendar.DATE, 1); 
	    dateobj=cal.getTime();
		return df.format(dateobj);
	}
	@Override
	public String getReport1(String s,int s1, String st1, String st2) {
		String ret;
		Date enddate=new Date();
		Date startdate=new Date();
		DateFormat df1 = new SimpleDateFormat("yyyyMMdd");
		int t1=0,t2=0,t3=0;
		double d1=0.0d,d2=0.0d;
		report3 rep1=new report3();
		List<OrderReport> temp=new ArrayList<OrderReport>();
		temp=CustomerService.getorderlist();
		List<OrderReport> temp1=new ArrayList<OrderReport>();
		
		try {
			 startdate=df1.parse(st1);
			 enddate=df1.parse(st2);
		} catch (ParseException e) {
			
			return "[]";
		}
		Calendar start = Calendar.getInstance();
		start.setTime(startdate);
		Calendar end = Calendar.getInstance();
		end.setTime(enddate);
	try{
		if(s1==703)
		{
			
		rep1.setFrid(s1);
		rep1.setName("Revenue report");
		
		for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1),date = start.getTime())
		{
			
			 String g=df1.format(date);
			
		
			for(OrderReport f:temp)
			{
				 String h=f.getOrder_date();
				  
					
				if(g.equals(h)&&s.equals(f.getFarm_info().getfid()))
				{
					
					t1=t1+1;
					String st=f.getStatus();
							if(st.equals("cancelled"))
							{
								t2=t2+1;
							}
					if(st.equals("Delivered"))
					{
						
						t3=t3+1;
						d1=d1+f.getProducts_total();
						d2=d2+f.getDelivery_charge();
						
					}
					
				}
			}
			
			
		}
		rep1.setDelivery_revenue(d2);
		rep1.setOrders_cancelled(t2);
		rep1.setOrders_placed(t1);
		rep1.setDelivery_revenue(d1);
		rep1.setOrders_delivered(t3);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		 ret=gson.toJson(rep1);
		return ret;
		}
	}
	catch(Exception e)
	{
		return "[]";
	}
	try{
		if(s1==704)
		{
			for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1),date = start.getTime())
			{
				String g=df1.format(date);
				for(OrderReport f:temp)
				{
					if(g.equals(f.getOrder_date())&&s.equals(f.getFarm_info().getfid())){
						if(f.getStatus().equals("Delivered")){
							temp1.add(f);
						}
					}
			}
				

			}
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			 ret=gson.toJson(temp1);
			return ret;
		}
		return "[]";
	}
	catch(Exception e)
	{
		return "[]";
	}
	
	}
	@Override
	public String getReport1(String s, int s1) {
		
		String ret;
		int t1=0,t2=0,t3=0;
		double d1=0.0d,d2=0.0d;
		report3 rep1=new report3();
		List<OrderReport> temp=new ArrayList<OrderReport>();
		temp=CustomerService.getorderlist();
		List<OrderReport> temp1=new ArrayList<OrderReport>();
try{
		if(s1==703)
		{
		rep1.setFrid(s1);
		rep1.setName("Revenue report");
			for(OrderReport f:temp)
			{
				if(s.equals(f.getFarm_info().getfid()))
				{
					t1=t1+1;
					String st=f.getStatus();
							if(st.equals("cancelled"))
							{
								t2=t2+1;
							}
					if(st.equals("Delivered"))
					{
						t3=t3+1;
						d1=d1+f.getProducts_total();
						d2=d2+f.getDelivery_charge();
					}
					
				}
			}
		rep1.setDelivery_revenue(d2);
		rep1.setOrders_cancelled(t2);
		rep1.setOrders_placed(t1);
		rep1.setProducts_revenue(d1);
		rep1.setOrders_delivered(t3);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		 ret=gson.toJson(rep1);
		return ret;
		
		}
}
catch(Exception e)
{
	return "[]";
}
try{
		if(s1==704)
		{
		
				for(OrderReport f:temp)
				{
					if(s.equals(f.getFarm_info().getfid())){
						if(f.getStatus().equals("Delivered")){
							temp1.add(f);
						}
					}
			}
		}
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			 ret=gson.toJson(temp1);
			return ret;
}
catch(Exception e)
{
	return "[]";
}	
}
	public void trash() {
		farmdata.clear();
		proddet.clear();
		prodl.clear();
		delivery.clear();
		
	}
	
}
