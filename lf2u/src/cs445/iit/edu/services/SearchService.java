package cs445.iit.edu.services;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cs445.iit.edu.BoundaryInterfaces.SearchBoundaryInterface;
import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.OrderReport;

public class SearchService implements SearchBoundaryInterface {
	
	String Result;
	List<FarmerDetails> col=new ArrayList<FarmerDetails>();
	 List<CustomerDetails> customer=new ArrayList<CustomerDetails>();
	 List<OrderReport> ordreport=new ArrayList<OrderReport>();
	 Gson gson=new GsonBuilder().setPrettyPrinting().create();

	public String Searchf(String key)
	{
		List<FarmerDetails> temp=new ArrayList<FarmerDetails>();
		boolean a1;
		
		for(FarmerDetails f2:FarmerService.getFarmList())
		{
			
	         
	         a1= Pattern.compile(Pattern.quote(key), Pattern.CASE_INSENSITIVE).matcher(gson.toJson(f2)).find();
	         if(a1=true)
	         {
	        	 temp.add(f2);
	         }
		}
		
		
		 return gson.toJson(temp);
       
		
	}
	public String Searchc(String key)
	{
		List<CustomerDetails> temp=new ArrayList<CustomerDetails>();
		String use;
		 
		customer=CustomerService.getcustomer();
		for(CustomerDetails f2:customer)
		{
			Gson f1=new GsonBuilder().setPrettyPrinting().create();
	         use=f1.toJson(f2);
	         boolean a1= Pattern.compile(Pattern.quote(key), Pattern.CASE_INSENSITIVE).matcher(use).find();
	         if(a1=true)
	         {
	        	 temp.add(f2);
	         }
		}
	        
	         use=gson.toJson(temp);
	         return use;
		
	}
	public String Searcho(String key)
	{
		List<OrderReport> temp=new ArrayList<OrderReport>();	
		String use;
		
		ordreport=CustomerService.getorderlist();
		for(OrderReport f2:ordreport)
		{
			
	         use=gson.toJson(f2);
	         boolean a1= Pattern.compile(Pattern.quote(key), Pattern.CASE_INSENSITIVE).matcher(use).find();
	         if(a1=true)
	         {
	        	 temp.add(f2);
	         }
		}
	 
	         return gson.toJson(temp);
		
	}
	@Override
	public String search(String strang, String strang2) {
		String Result;
		if(strang.equals("farm"))
		{
			Result=Searchf(strang2);
		}
		else if (strang.equals("customer"))
		{
			Result=Searchc(strang2);
		}
		else if(strang.equals("order"))
		{
			Result=Searcho(strang2);
		}
		else
		{
			Result="wrong";
		}
		return Result;
	}
	
}
