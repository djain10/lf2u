package cs445.iit.edu.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cs445.iit.edu.BoundaryInterfaces.ManagerBoundaryInterface;
import cs445.iit.edu.models.By_farmer;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.Manageraccount;
import cs445.iit.edu.models.OrderReport;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.models.gcpid;
import cs445.iit.edu.models.managereport12;
import cs445.iit.edu.models.managerreport;
import cs445.iit.edu.models.managerreport45;
import cs445.iit.edu.models.managerreportlis;

public class ManagerService implements ManagerBoundaryInterface {

	managerreportlis manager = new managerreportlis();
	Gson gson = new GsonBuilder().setPrettyPrinting().create();

	public static List<CatalogView> catalogManager = new ArrayList<CatalogView>();

	public static List<CatalogView> getlist() {
		return catalogManager;
	}

	// manager report list
	public String getreplist() {
		List<managerreport> mh = new ArrayList<managerreport>();
		manager.setRepList();
		mh = manager.getReportManager();
		return new GsonBuilder().setPrettyPrinting().create().toJson(mh);

	}

	// manager account list
	@Override
	public String getmlist() {
		
		List<Manageraccount> man = new ArrayList<Manageraccount>();
		manager.setAccount();
		man = manager.getAccount();
		return new GsonBuilder().setPrettyPrinting().create().toJson(man);
	}

	// manager with an id
	@Override
	public String getmanager(int mid) {
	
		List<Manageraccount> man1 = new ArrayList<Manageraccount>();

		manager.setAccount();
		
		for (Manageraccount t : manager.getAccount()) {
			if (t.getmid() == mid) {
				man1.add(t);

			}

		}
		return new GsonBuilder().setPrettyPrinting().create().toJson(man1);
	}

	public String getDate() {

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -1);
		return df.format(cal.getTime());
	}

	public String getMonth() {

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -1);
		return df.format(cal.getTime());
	}

	public String getCurrentDate() {
		DateFormat datef = new SimpleDateFormat("yyyyMMdd");
		return datef.format(new Date());
	}

	// manager report type 1 and 2
	@Override
	public String getManagerReport1(int oneid, String strang, String strang2) {
		String output;
		String getit;
		int calc = 0, calc1 = 0, calc2 = 0, calc3 = 0;

		managereport12 manage12 = new managereport12();
		List<OrderReport> temp1 = new ArrayList<OrderReport>();
		temp1 = CustomerService.getorderlist();
		try {
			if (oneid == 1) {
				getit = getCurrentDate();
				manage12.setMrid(oneid);
				manage12.setName("today order sire");
			} else {
				getit = getDate();
				manage12.setName("Yesterdays orders");
				manage12.setMrid(oneid);
			}
			if (strang == null || strang2 == null) {

				for (OrderReport k : temp1) {
					String dat = k.getOrder_date();

					if (getit.equals(dat)) {

						calc += calc + 1;

						if (k.getStatus().equals("open")) {
							calc1 = calc1 + 1;
						} else if (k.getStatus().equals("Delivered")) {
							calc2 = calc2 + 1;
						} else {
							calc3 = calc3 + 1;
						}

					}
				}
				manage12.setOrders_placed(calc);
				manage12.setOrders_delivered(calc2);
				manage12.setOrders_open(calc1);
				manage12.setOrders_cancelled(calc3);
			}

			else {
				Date endDate = new Date();
				Date startDate = new Date();
				DateFormat df1 = new SimpleDateFormat("yyyyMMdd");
				try {
					startDate = df1.parse(strang);
					endDate = df1.parse(strang2);
				} catch (ParseException e) {

					e.printStackTrace();
				}

				Calendar calen = Calendar.getInstance();
				calen.setTime(startDate);
				Calendar end = Calendar.getInstance();
				end.setTime(endDate);
				for (Date date = calen.getTime(); calen.before(end); calen.add(Calendar.DATE,
						1), date = calen.getTime()) {
					
					for (OrderReport k : temp1) {
						if (df1.format(date).equals(k.getOrder_date())) {
							calc = calc + 1;

											if (k.getStatus().equals("open")) {
								calc1 = calc1 + 1;
							} else if (k.getStatus().equals("Delivered")) {
								calc2 = calc2 + 1;
							} else {
								calc3 = calc3 + 1;
							}

						}
					}
				}
				manage12.setOrders_placed(calc);
				manage12.setOrders_delivered(calc2);
				manage12.setOrders_open(calc1);
				manage12.setOrders_cancelled(calc3);
			}
		} catch (Exception e) {
			return "[]";
		}

		return gson.toJson(manage12);
	}

	// manager report type 4 and 3
	@Override
	public String getManagerReport2(int oneid, String strang, String strang2) {
		String  c, c2, st, ed;
		List<FarmerDetails> famrd1 = new ArrayList<FarmerDetails>();
		List<OrderReport> famrd2 = new ArrayList<OrderReport>();
		famrd1 = FarmerService.getFarmList();
		famrd2 = CustomerService.getorderlist();
		List<By_farmer> byfarmer = new ArrayList<By_farmer>();
		int calc = 0, calc1 = 0, calc2 = 0, calc3 = 0;
		int calc4 = 0, calc5 = 0, calc6 = 0, calc7 = 0;
		double data = 0.0, data1 = 0.0, data2 = 0.0, data3 = 0.0, data4 = 0.0;
		double data5 = 0.0d, data6 = 0.0d, data7 = 0.0d, data8 = 0, data9 = 0.0d;
		managerreport45 rep1 = new managerreport45();
		try {
			if (strang == null || strang2 == null) {
				if (oneid == 3) {
					st = getMonth();
					ed = haveDay();
					Date enddate = new Date();
					Date startdate = new Date();
					DateFormat datef1 = new SimpleDateFormat("yyyyMMdd");
					try {
						startdate = datef1.parse(st);
						enddate = datef1.parse(ed);
					} catch (ParseException e) {

						e.printStackTrace();
					}

					Calendar start = Calendar.getInstance();
					start.setTime(startdate);
					Calendar end = Calendar.getInstance();
					end.setTime(enddate);
					for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE,
							1), date = start.getTime()) {
						String c8 = datef1.format(date);

						for (FarmerDetails f : famrd1) {
							By_farmer farc = new By_farmer();
							c = f.getfid();
							c2 = f.getFarm_info().getName();
							for (OrderReport r : famrd2) {

								r.getOrder_date();
								String c7 = r.getFarm_info().getfid();

								if (c8.equals(r.getOrder_date()) && c7.equals(c)) {
									rep1.setName("CustomerName");
									farc.setName(c2);
									farc.setFid(c);
									calc3 = calc3 + 1;
									if (r.getStatus().equals("open")) {
										calc = calc + 1;
									} else if (r.getStatus().equals("cancelled")) {
										calc1 = calc1 + 1;
									} else {
										calc2 = calc2 + 1;
									}
									data = data + r.getOrder_total();
									data1 = data1 + r.getDelivery_charge();
									data2 = data2 + r.getProducts_total();
									data3 = data * 0.03;
									data4 = data - data3;

								}
							}
							farc.setOrders_placed(calc3);
							farc.setOrders_delivered(calc2);
							farc.setOrders_open(calc);
							farc.setOrders_cancelled(calc1);
							farc.setTotal_revenue(calc);
							farc.setDelivery_revenue(data1);
							farc.setProducts_revenue(data2);
							farc.setLftu_fees(data3);
							farc.setPayable_to_farm(data4);
							if (farc.getProducts_revenue() == 0.0d) {
								continue;
							} else {
								byfarmer.add(farc);
							}
						}
						calc4 = calc4 + calc3;
						calc7 = calc7 + calc;
						calc5 = calc5 + calc1;
						calc6 = calc6 + calc2;
						data5 = data5 + data;
						data6 = data6 + data1;
						data7 = data7 + data2;
						data8 = data8 + data3;
						data9 = data9 + data4;
					}
					rep1.setMrid(oneid);
					rep1.setName("foregoing revenue for  month");
					rep1.setBy_farmer(byfarmer);
					rep1.setOrders_placed(calc4);
					rep1.setOrders_delivered(calc6);
					rep1.setOrders_open(calc7);
					rep1.setOrders_cancelled(calc5);
					rep1.setTotal_revenue(data5);
					rep1.setTotal_delivery_revenue(data6);
					rep1.setTotal_products_revenue(data7);
					rep1.setTotal_lftu_fees(data8);
					rep1.setTotal_payable_to_farms(data9);
									
					return gson.toJson(rep1); 

				} else if (oneid == 4 || oneid == 5) {
					String key = getDate();

					for (FarmerDetails f : famrd1) {

						By_farmer farc = new By_farmer();
												
						for (OrderReport r : famrd2) {

							r.getOrder_date();
							

							if (key.equals(r.getOrder_date()) && r.getFarm_info().getfid().equals(f.getfid())) {

								farc.setName(f.getFarm_info().getName());
								farc.setFid(f.getfid());
								calc3 = calc3 + 1;
								if (r.getStatus().equals("open")) {
									calc = calc + 1;
								} else if (r.getStatus().equals("cancelled")) {
									calc1 = calc1 + 1;
								} else {
									calc2 = calc2 + 1;
								}
								data = data + r.getOrder_total();
								data1 = data1 + r.getDelivery_charge();
								data2 = data2 + r.getProducts_total();
								data3 = data * 0.03;
								data4 = data - data3;

							}
						}
						farc.setOrders_placed(calc3);
						farc.setOrders_delivered(calc2);
						farc.setOrders_open(calc);
						farc.setOrders_cancelled(calc1);
						farc.setTotal_revenue(calc);
						farc.setDelivery_revenue(data1);
						farc.setProducts_revenue(data2);
						farc.setLftu_fees(data3);
						farc.setPayable_to_farm(data4);
						if (farc.getProducts_revenue() == 0.0d) {
							continue;
						} else {
							byfarmer.add(farc);
						}
						calc4 = calc4 + calc3;
						calc7 = calc7 + calc;
						calc5 = calc5 + calc1;
						calc6 = calc6 + calc2;
						data5 = data5 + data;
						data6 = data6 + data1;
						data7 = data7 + data2;
						data8 = data8 + data3;
						data9 = data9 + data4;
					}

					rep1.setMrid(oneid);
					rep1.setName("Revenue yesterday");
					rep1.setBy_farmer(byfarmer);
					rep1.setOrders_placed(calc4);
					rep1.setOrders_delivered(calc6);
					rep1.setOrders_open(calc7);
					rep1.setOrders_cancelled(calc5);
					rep1.setTotal_revenue(data5);
					rep1.setTotal_delivery_revenue(data6);
					rep1.setTotal_products_revenue(data7);
					rep1.setTotal_lftu_fees(data8);
					rep1.setTotal_payable_to_farms(data9);
					
					return  gson.toJson(rep1);

				}
			} else {

				Date enddate = new Date();
				Date startdate = new Date();
				DateFormat df1 = new SimpleDateFormat("yyyyMMdd");
				try {
					startdate = df1.parse(strang);
					enddate = df1.parse(strang2);
				} catch (ParseException e) {

					e.printStackTrace();
				}

				Calendar start = Calendar.getInstance();
				start.setTime(startdate);
				Calendar end = Calendar.getInstance();
				end.setTime(enddate);
				for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE,
						1), date = start.getTime()) {
					String c8 = df1.format(date);

					for (FarmerDetails f : famrd1) {
						By_farmer farc = new By_farmer();
						c = f.getfid();
						c2 = f.getFarm_info().getName();
						for (OrderReport r : famrd2) {

							r.getOrder_date();
							String c7 = r.getFarm_info().getfid();

							if (c8.equals(r.getOrder_date()) && c7.equals(c)) {
								rep1.setName("came");
								farc.setName(c2);
								farc.setFid(c);
								calc3 = calc3 + 1;
								if (r.getStatus().equals("open")) {
									calc = calc + 1;
								} else if (r.getStatus().equals("cancelled")) {
									calc1 = calc1 + 1;
								} else {
									calc2 = calc2 + 1;
								}
								data = data + r.getOrder_total();
								data1 = data1 + r.getDelivery_charge();
								data2 = data2 + r.getProducts_total();
								data3 = data * 0.03;
								data4 = data - data3;

							}
						}
						farc.setOrders_placed(calc3);
						farc.setOrders_delivered(calc2);
						farc.setOrders_open(calc);
						farc.setOrders_cancelled(calc1);
						farc.setTotal_revenue(calc);
						farc.setDelivery_revenue(data1);
						farc.setProducts_revenue(data2);
						farc.setLftu_fees(data3);
						farc.setPayable_to_farm(data4);
						if (farc.getProducts_revenue() == 0.0d) {
							continue;
						} else {
							byfarmer.add(farc);
						}
					}
					calc4 = calc4 + calc3;
					calc7 = calc7 + calc;
					calc5 = calc5 + calc1;
					calc6 = calc6 + calc2;
					data5 = data5 + data;
					data6 = data6 + data1;
					data7 = data7 + data2;
					data8 = data8 + data3;
					data9 = data9 + data4;
				}
				rep1.setMrid(oneid);
				rep1.setName("Revenue for the range " + strang + " " + strang2);
				rep1.setBy_farmer(byfarmer);
				rep1.setOrders_placed(calc4);
				rep1.setOrders_delivered(calc6);
				rep1.setOrders_open(calc7);
				rep1.setOrders_cancelled(calc5);
				rep1.setTotal_revenue(data5);
				rep1.setTotal_delivery_revenue(data6);
				rep1.setTotal_products_revenue(data7);
				rep1.setTotal_lftu_fees(data8);
				rep1.setTotal_payable_to_farms(data9);				
				return gson.toJson(rep1);

			}
		} catch (Exception e) {
			return "[]";
		}
		return "[]";
	}

	@Override
	public String increCatalog(CatalogView use) {

		/*try {
			catalogManager.add(use); 
			gcpid gcpid = new gcpid();
				gcpid.set(use.getGcpid());
			return new GsonBuilder().setPrettyPrinting().create().toJson(gcpid);
		} catch (Exception e) {
			return "[]";
		}
	}*/
		String out;
		try
		{
		//adding to a list to have concolidated one.
		catalogManager.add(use);
		
		gcpid g=new gcpid();
		//to print the json
		//sending json out
		String p=use.getGcpid();
		g.set(p);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		 out=gson.toJson(g);
		 return out;	
		}
		catch(Exception e)
		{
			return "[]";
		}
		}

	// catalog list
	@Override
	public String haveCatalog() {
		String out;
		Gson f1 = new GsonBuilder().setPrettyPrinting().create();
		 out=f1.toJson(catalogManager);
		 return out;
	}

	// catalog search
	@Override
	public boolean updateCatalog(String strang, CatalogView catmanage) {
		boolean update = false;
		try {
			for (CatalogView t : catalogManager) {
				if (t.getGcpid().equals(strang)) {
					int i = catalogManager.indexOf(t);
					catmanage.setgcpid(t.getGcpid());
					catalogManager.set(i, catmanage);
					update = true;
				}
			}
			return update;
		} catch (Exception e) {
			return false;
		}
	}

	public static String haveDay() {

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		return df.format(cal.getTime());
	}

	public void trash() {
		catalogManager.clear();

	}

}