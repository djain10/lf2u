package cs445.iit.edu.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cs445.iit.edu.BoundaryInterfaces.DeliveryBoundaryInterface;
import cs445.iit.edu.models.OrderReport;
import cs445.iit.edu.models.corders;
import cs445.iit.edu.models.Orderget;

public class DeliveryService implements DeliveryBoundaryInterface {
    public static List<corders> temp1=new ArrayList<corders>();
    public static List<Orderget> temp2=new ArrayList<Orderget>();
    public static List<OrderReport> temp3=new ArrayList<OrderReport>();
    public  String getcurrentdate()
    {
        DateFormat df = new SimpleDateFormat("yyyyMMdd ");
        Date dateobj = new Date();
        return df.format(dateobj);
    }
    @Override
    public String Delivery(String id) {
        String a="no";
        String date=getcurrentdate();
        
        temp1=CustomerService.getcorder();
        temp2=CustomerService.getorderget();
        temp3=CustomerService.getorderlist();
        String s1,s2,s3,s4;
        //setting orderc status
        try{
            
        
        for(corders s:temp1)
        { s1=s.getOid();
        if(s1.equals(id))
        {
    
            s.setStatus("Delivered");
            s.setactual(date);
            for(Orderget f:temp2)
            {
                s2=f.getoid();
                        if(s2.equals(id))
                        {
                            f.setorders(s);
                            a="done";                }
            }
            //setting in the master account order
            for(OrderReport d:temp3)
            {
                
                    d.setStatus("Delivered");
                    d.setActual_delivery_date(date);
            }
        
        }
        CustomerService.setcorder(temp1);
        CustomerService.setorderget(temp2);
        CustomerService.setorderlist(temp3);
        return a;
        }
        }
        
        catch(Exception e)
        {
            return "[]";
        }
        return "can";
    }
    }