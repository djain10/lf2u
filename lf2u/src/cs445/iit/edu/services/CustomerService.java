package cs445.iit.edu.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cs445.iit.edu.BoundaryInterfaces.CustomerBoundaryInterface;
import cs445.iit.edu.models.CustomerDetails;
import cs445.iit.edu.models.Delivery;
import cs445.iit.edu.models.Farm_info;
import cs445.iit.edu.models.FarmerDetails;
import cs445.iit.edu.models.OrderReport;
import cs445.iit.edu.models.Order_detail;
import cs445.iit.edu.models.Order_details;
import cs445.iit.edu.models.Productdetails;
import cs445.iit.edu.models.CatalogView;
import cs445.iit.edu.models.cid;
import cs445.iit.edu.models.corders;
import cs445.iit.edu.models.oid;
import cs445.iit.edu.models.Orderget;
import cs445.iit.edu.models.PlaceOrder;
import cs445.iit.edu.models.Corroborate;

public class CustomerService implements CustomerBoundaryInterface {
public static List<CustomerDetails> cdetails=new ArrayList<CustomerDetails>();
public static List<PlaceOrder> plcord=new ArrayList<PlaceOrder>();
public static List<corders> cord=new ArrayList<corders>();
public static List<Orderget> orders=new ArrayList<Orderget>();
public static List<OrderReport> orderReport=new ArrayList<OrderReport>();
List<corders> customerOrders=new ArrayList<corders>();

 public static List<PlaceOrder> getplaceorder()
 {
 	return plcord;
 }
 public static List<CustomerDetails> getcustomer()
 {
 	return cdetails;
 }
 public static List<corders> getcorder()
{
	return cord;
}
public static List<Orderget> getorderget()
{
	return orders;
}
public static void setcorder(List<corders> s)
{
	cord=s;
}
public static void setorderget(List<Orderget> s)
{
	orders=s;
}
public static List<OrderReport> getorderlist() {
	
	return orderReport;
}
public static void setorderlist(List<OrderReport> s)
{
	orderReport=s;
}

	@Override
	public String createAccount(CustomerDetails customer) {
		if(customer==null) return "";
		else {
			cdetails.add(customer);
			cid g=new cid();
			g.set(customer.getcid());
			return new GsonBuilder().setPrettyPrinting().create().toJson(g);
		}
	}
	@Override
	public boolean update(String id, CustomerDetails cDetails) {
		boolean update=false;
		try{
			for(CustomerDetails customer:cdetails)
				if(customer.getcid().equals(id))
				{	cDetails.setcid(id);
				cdetails.set(cdetails.indexOf(customer),cDetails);
					update=true;
					break;
				}
		return update;
		}
		catch(Exception e){ 
			return false;
		}
	}
	public   String getcurrentdate()
	{
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date dateobj = new Date();
		return df.format(dateobj);
	}
	@Override
	public String getCustomer(String s) {
		for(CustomerDetails obj:cdetails)
			if(obj.getcid().equals(s))
				return new GsonBuilder().setPrettyPrinting().create().toJson(obj);
		 return "[]";
	}
	
	public boolean verifycustomer(String id)
	{
		boolean trueCustomer=false;
		//if(id==null)return trueCustomer;
		for(CustomerDetails obj:cdetails)
			if(obj.getcid().equals(id)){
				trueCustomer=true;
				break;
			}
		return trueCustomer;	
	}

public void trash()
{
	cdetails.clear();
	cord.clear();
	cord.clear();
	orders.clear();
	orderReport.clear();
}

	
	@Override
	public String createOrder(String s, PlaceOrder p) {
	      String ret,calc,calc2,calc3,calc4,calc5,calc6,calc7,calc8,calc9,calc10,calc11,calc12;
	      String[] zip=null;
	      boolean bool1=false,bool2=true;
	      double darn1,darn2,darn3,darn4=0.0d,darn5=0.0d,darn6;
	      Farm_info farminf=new Farm_info();
	      List<FarmerDetails> fdata=new ArrayList<FarmerDetails>();
	      List<Productdetails> fdata2=new ArrayList<Productdetails>();
	      List<Delivery> fdata3=new ArrayList<Delivery>();
	      List<CatalogView> fdata4=new ArrayList<CatalogView>();
	      List<Order_details> orderlist = new ArrayList<Order_details>();
	      
	      
	      bool2=verifycustomer(s);
	      if(bool2==false) {
	    	  return "[]";
	      }
	      
	      Order_detail[] order;
	     
	      OrderReport rep=new OrderReport();
	      
	    plcord.add(p);
	  	String t=p.getFid();
      	String t1=p.getoid();
	    //checking the zip code
      	fdata=FarmerService.getFarmList();
	      calc9=p.getFid();
	      try {
      	  for(FarmerDetails d:fdata)
      	  {
      		  calc10=d.getfid();
      		  if(calc9.equals(calc10))
      		  {
      			 zip=d.getDelivers_to(); 
      		  }
      		  
      	  }
      	  for(CustomerDetails h:cdetails)
      	  {
      		  calc11=h.getcid();
      		  if(calc11.equals(s))
      		  {
      			  calc12=h.getZip();
      			for(String zips:zip)
      			{
      				if(zips.equals(calc12))
      				{
      					bool1=true;
      				}
      			}
            	  if(bool1==false)
            	  {
            		  return "no zip";
            	  }
      		  }
      	  }
      	
	      	corders o=new corders();
	      
	      	o.setFid(t);
	      	o.setOid(t1);
	      	cord.add(o);
	      	Orderget k=new Orderget();
	      	k.set(s);
	      	k.setoid(t1);
	      	k.setorders(o);
	      	orders.add(k);	        
	      	 rep.setOid(t1);
	      	 rep.setOrder_date(o.getOrder_date());
	      	 rep.setPlanned_delivery_date(o.getPlanned_delivery_date());
	      	 rep.setActual_delivery_date(o.getActual_delivery_date());
	      	 rep.setStatus(o.getStatus());	      	 
	      	fdata2=FarmerService.getProductList();
	      	fdata3=FarmerService.getDeliveryList();
	      	fdata4=ManagerService.getlist();
	      order=p.getOrder_detail();
	      	 for(FarmerDetails fa:fdata)
	      	 {
	      		 calc=fa.getfid();
	      		 if(calc.equals(t))
	      		 {
	      			 farminf=fa.getFarm_info();
	      		 }
	      	 }
	      	
	      	 farminf.setfid(t);
	      	 rep.setFarm_info(farminf);
	      	 rep.setDelivery_note(p.getDelivery_note());
	      	 for(Delivery dp:fdata3)
	      	 {
	      		 calc2=dp.getid();
	      		 if(calc2.equals(t))
	      		 {
	      			 darn5=dp.getcharges();
	      			 rep.setDelivery_charge(darn5);
	      		 }
	      	 }
	      	 for(Order_detail h:order)
	      	 {
	      		Order_details repo=new Order_details();
	      		 calc4=h.getFspid();
	      		 darn1=h.getamount();
	      	 
	      	 for(Productdetails gg:fdata2)
	      	 {
	      		 calc3=gg.getfspid();
	      		 if(calc3.equals(calc4))
	      		 {
	      			 repo.setFspid(calc3);
	      			 darn2=gg.getPrice();
	      			 darn3=darn2*darn1;
	      			 repo.setLine_item_total(darn3);
	      			 darn4+=darn3;   
	      			//kara h change yaha pr
	      			 calc5=gg.getPrice()+gg.getProduct_unit();
	      			 repo.setPrice(calc5);
	      			 calc6=darn1+" "+gg.getProduct_unit();
	      			 repo.setOrder_size(calc6);
	      			 calc8=gg.getGcpid();
	      			 for(CatalogView lk:fdata4)
	      			 {
	      				 calc7=lk.getGcpid();
	      				 if(calc8.equals(calc7))
	      				 {
	      					 repo.setName(lk.getName());				 	 
	      					 
	      				 }
	      			 }
	      		 } 
	      		
	      	 }
	      	 orderlist.add(repo);
	      	 }
	      	 rep.setOrder_detail(orderlist);
	      	 rep.setProducts_total(darn4);
	      	 
	      	 rep.setOrder_total(darn4+darn5);
			 
	      }
	      catch(Exception e)
	      {
	    	  return "[]";
	      }
		      	oid o1=new oid();
		      	o1.set(t1);
	
	         	orderReport.add(rep);
	
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
			 return gson.toJson(o1);		
	      }
	

@Override
public String showOrder(String id) {

	String output;
	String cout;
	
	try{
		for(Orderget p:orders){
	
	
		cout=p.getcid();
		if(cout.equals(id))
		{
			customerOrders.add(p.getorders());
			
		}
	}
	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	 output=gson.toJson(customerOrders);
}
		 catch(Exception e)
		 {
			 return "[]";
	 }
	 return output;
}

	
	
	@Override
public String cancelOrder(String s, String oid) {
		try {
			for (corders obj : cord) {
				if (obj.getOid().equals(oid)) {
					if (obj.getStatus().equals("open")) {
						if (getcurrentdate().equals(obj.getOrder_date())) {
							obj.setStatus("cancelled");
							Corroborate support = new Corroborate();
							support.Support("cancelled");
							for (OrderReport t : orderReport) 
								if (t.getOid().equals(oid)) 
									t.setStatus("cancelled");
							return new GsonBuilder().setPrettyPrinting().create().toJson(support);
						} else {
							return "nada";
						}
					} else {
						return "done";
					}
				}
			}
		} catch (Exception e) {
			return "[]";
		}
		return "[]";
}	 
	

@Override
public String getOrderDetails(String cid, String oid) {
	
	for(Orderget k:orders)
	{
		if(k.getcid().equals(cid)&&k.getoid().equals(oid)){
			for(OrderReport o:orderReport)
				if(o.getOid().equals(oid))
					return new GsonBuilder().setPrettyPrinting().create().toJson(o);
		}else{
			return "invalid order and customer id";
		}
	}
	
	return"[]";
}
}

	

	

	

