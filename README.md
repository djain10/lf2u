ReadMe

Configuration Instructions:
	Download and install jdk
	Check JAVA_HOME and set the path if not set
	Download and install apache-ant-1.9.7
	Check ANT_HOME and set path if not set.
	sudo apt-get install git
	Download INstall Tomcat-8.0.33


Build and Deploy INstructions:
	
	I have provided a script named as running.sh and ccn-script.sh. FOr second shell script project shpuld be downloaded first.
	 It will download and install all the above configuration instructions,setting up the environment variables, then cloning therepository.
	 It will create executable .war file inside the project directory
	 It will copy the .war file into webapps directory of apache-tomcat-8.0.39
	Start the server.

 Credits and acknowledgements

	The project credit goes to the following person for proper guidance.	
   1) Professor Virgil Bistriceanu
   2) Alexandru Orhean

If  shell script doesnt run:
	
Note
To run the project the war file has to be copied into the webapps folder of tomcat. I've included a
tomcat 8 with the zip folder. The url is https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.39/bin/apache-tomcat-8.0.39.zip
 Unzipping will get the tomcat ready and using ./bin/startup.sh will get the tomcat running.
The repositories referred were Delectable from previous semester. as well as the following ones:
https://github.com/asarcastichawk/CS445
https://github.com/Bhasheyam/lf2u
https://github.com/cdani-S16/Restaurant-as-a-REST-Service

TO do all the building of files and all traverse inside the project where build.xml is present.
	
	ant -f build.xml.xml
	ant -"Customer" -f build.xml
	ant -"FarmerUnitTest" -f build.xml
	ant -"ManagerUnitTest" -f build.xml
	ant -"DeliveryUnitTest" -f build.xml
	ant -"SearchUnitTest" -f build.xml
	ant -"junitreport" -f build.xml
	ant -"report" -f build.xml


Other than Maven is also used to show Cyclomatic comlexity.
go where the pom.xml is present and run following command.
	mvn pmd:pmd
	mvn javancss:report

Reports generated can be seen in javancss.html in traget folder and junit folder contains coverage and all the things.
unit/CoverageReports.